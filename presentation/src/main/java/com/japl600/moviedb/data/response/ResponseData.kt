package com.japl600.moviedb.data.response

/**
 * Created by usuario on 18/03/2018.
 */

/**
 * Credit consultation data
 */
class PopularMovieResponse @JvmOverloads constructor(
        var page : Int = 0,
        var total_results : Int = 0,
        var total_pages : Int = 0,
        var results: MutableList<PopularMovie> = mutableListOf()
)


class PopularSeriesResponse @JvmOverloads constructor(
        var page : Int = 0,
        var total_results : Int = 0,
        var total_pages : Int = 0,
        var results: MutableList<PopularSerie> = mutableListOf()
)

class PopularActorsResponse @JvmOverloads constructor(
        var page : Int = 0,
        var total_results : Int = 0,
        var total_pages : Int = 0,
        var results: MutableList<PopularActor> = mutableListOf()
)

class PopularMovie @JvmOverloads constructor(
        var vote_count : Int = 0,
        var id : Int = 0,
        var vote_average : Double = 0.0,
        var title : String = "",
        var popularity : Double = 0.0,
        var overview : String = "",
        var poster_path : String = ""
)

class PopularSerie @JvmOverloads constructor(
        var vote_count : Int = 0,
        var id : Int = 0,
        var vote_average : Double = 0.0,
        var name : String = "",
        var popularity : Double = 0.0,
        var overview : String = "",
        var poster_path : String = ""
)

class PopularActor @JvmOverloads constructor(
        var popularity : Double = 0.0,
        var id : Int = 0,
        var profile_path : String = "",
        var name : String = ""
)

class MovieResponse @JvmOverloads constructor(
        var id : Int = 0,
        var budget : Int = 0,
        var vote_average : Double = 0.0,
        var title : String = "",
        var popularity : Double = 0.0,
        var overview : String = "",
        var poster_path : String = "",
        var backdrop_path : String = ""
)

class SeriesResponse @JvmOverloads constructor(
        var vote_count : Int = 0,
        var id : Int = 0,
        var vote_average : Double = 0.0,
        var name : String = "",
        var popularity : Double = 0.0,
        var overview : String = "",
        var poster_path : String = "",
        var backdrop_path : String = ""
)

class ActorResponse @JvmOverloads constructor(
        var birthday : String = "",
        var id : Int = 0,
        var name : String = "",
        var biography : String = "",
        var popularity : Double = 0.0,
        var place_of_birth : String = "",
        var profile_path : String = ""
)
