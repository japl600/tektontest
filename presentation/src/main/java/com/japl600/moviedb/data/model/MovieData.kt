package com.japl600.moviedb.data.model

import com.japl600.moviedb.data.response.PopularMovie

/**
 * Created by usuario on 19/03/2018.
 */

/**
 * Credit consultation data
 */
class ItemMovie @JvmOverloads constructor(
        var title : String = "",
        var subtitle : String = ""
)