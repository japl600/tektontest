package com.japl600.moviedb.data;

import com.japl600.moviedb.data.response.ActorResponse;
import com.japl600.moviedb.data.response.MovieResponse;
import com.japl600.moviedb.data.response.PopularActorsResponse;
import com.japl600.moviedb.data.response.PopularMovieResponse;
import com.japl600.moviedb.data.response.PopularSeriesResponse;
import com.japl600.moviedb.data.response.SeriesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * Created by usuario on 18/03/2018.
 */

public interface MetodosWS {
    String api_key = "?api_key=ddeff33912ae14df59b1dc36c3a0aaf3";

    @GET("movie/popular"+api_key)
    Call<PopularMovieResponse> getPopularMovies(@Header("page") String page,
                                                @Header("language") String language);
    @GET
    Call<MovieResponse> getMovieDetail2(@Url String url);

    @GET
    Call<PopularSeriesResponse> getPopularSeries(@Url String url);

    @GET
    Call<SeriesResponse> getSeriesDetail(@Url String url);

    @GET
    Call<PopularActorsResponse> getPopularActors(@Url String url);

    @GET
    Call<ActorResponse> getActorsDetail(@Url String url);
}
