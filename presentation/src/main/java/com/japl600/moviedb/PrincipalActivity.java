package com.japl600.moviedb;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.japl600.moviedb.databinding.ActivityPrincipalBinding;
import com.japl600.moviedb.view.fragment.ActorDetailFragment;
import com.japl600.moviedb.view.fragment.ActorsFragment;
import com.japl600.moviedb.view.fragment.MovieDetailFragment;
import com.japl600.moviedb.view.fragment.MoviesFragment;
import com.japl600.moviedb.view.fragment.SeriesDetailFragment;
import com.japl600.moviedb.view.fragment.SeriesFragment;

/**
 * Created by usuario on 17/03/2018.
 */

public class PrincipalActivity extends AppCompatActivity {

    private ActivityPrincipalBinding binding;

    private Context mContext;
    private ProgressDialog progress;
    private ActionBarDrawerToggle mDrawerToggle;
    private boolean mTwoPane = true;
    private ArrayAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView( PrincipalActivity.this, R.layout.activity_principal);
        mContext = this;

        //Toolbar
        setSupportActionBar(binding.appBar);

        //Portrait or landscape
        mTwoPane = (binding.nvMenu != null)?false:true;
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(!mTwoPane){
            //drawer
            mDrawerToggle = new ActionBarDrawerToggle(this,
                    binding.dlMenu,
                    binding.appBar,
                    R.string.app_name,
                    R.string.app_name) {

                /** Called when a drawer has settled in a completely closed state. */
                public void onDrawerClosed(View view) {
                    super.onDrawerClosed(view);
                    invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                }

                /** Called when a drawer has settled in a completely open state. */
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                }
            };

            // Set the drawer toggle as the DrawerListener
            binding.dlMenu.setDrawerListener(mDrawerToggle);
            mDrawerToggle.syncState();

            //expandable list data
            //prepareListData();
            String [] mStringOfPlanets = getResources().getStringArray(R.array.menu_array);

            listAdapter = new ArrayAdapter<String>(this, R.layout.list_item, mStringOfPlanets);

            // setting list adapter
            binding.elvGroup.setAdapter(listAdapter);
            // click event
            binding.elvGroup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    menuSelected(position);
                    return;
                }
            });

        }
    }

    private void menuSelected(int position) {
        Fragment fragment = null;
        String title = "";
        if (position == 0){
            title = "Popular Movies";
            fragment = new MoviesFragment();
        } else if (position == 1){
            title = "Popular Series";
            fragment = new SeriesFragment();
        } else if (position == 2){
            title = "Popular Actors";
            fragment = new ActorsFragment();
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    //.addToBackStack(null)
                    .commit();
            setTitle(title);
        }
    }

    public void detailMovie(int movie_id) {
        Fragment fragment = new MovieDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("movie_id", movie_id);
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.item_detail_container, fragment)
                .commit();
        setTitle("Movie Detail");
    }

    public void detailSerie(int serie_id) {
        Fragment fragment = new SeriesDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("serie_id", serie_id);
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.item_detail_container, fragment)
                .commit();
        setTitle("Series Detail");
    }

    public void detailActor(int actor_id) {
        Fragment fragment = new ActorDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("actor_id", actor_id);
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.item_detail_container, fragment)
                .commit();
        setTitle("Actor Detail");
    }
}
