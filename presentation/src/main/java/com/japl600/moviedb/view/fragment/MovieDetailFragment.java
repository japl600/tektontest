package com.japl600.moviedb.view.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.japl600.moviedb.R;
import com.japl600.moviedb.adapter.CustomMovieAdapter;
import com.japl600.moviedb.data.HelperWS;
import com.japl600.moviedb.data.MetodosWS;
import com.japl600.moviedb.data.response.MovieResponse;
import com.japl600.moviedb.data.response.PopularMovieResponse;
import com.japl600.moviedb.databinding.FragmentMovieDetailBinding;
import com.japl600.moviedb.tools.Constant;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by usuario on 19/03/2018.
 */

public class MovieDetailFragment extends Fragment {

    private Context mContext;
    private ProgressDialog progress;
    private FragmentMovieDetailBinding binding;

    private int movieID;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        progress = new ProgressDialog(mContext);
        progress.setCancelable(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_movie_detail, container, false);
        View view = binding.getRoot();

        //for future uses
        Bundle bundle = getArguments();
        movieID = bundle.getInt("movie_id");

        loadMovieDetail();

        //onClicksListeners
        //binding.btnCancelar.setOnClickListener(this);
        return view;
    }

    private void loadMovieDetail() {
        progress = new ProgressDialog(mContext);
        progress.setMessage("Loading...");
        progress.show();

        //Llamando al WS
        MetodosWS metodosWS = HelperWS.getConfiguration(mContext).create(MetodosWS.class);
        Call<MovieResponse> responseCall = metodosWS.getMovieDetail2("movie/"+movieID+"?api_key=ddeff33912ae14df59b1dc36c3a0aaf3&language=en-US");
        responseCall.enqueue(new Callback<MovieResponse>() {

            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                MovieResponse movieResponse = response.body();
                binding.title.setText(movieResponse.getTitle());
                binding.subTitle.setText(movieResponse.getOverview());
                Picasso.with(mContext).
                        load(Constant.IMG_ROUTE + movieResponse.getBackdrop_path()).into(binding.ivBackDrop);
                progress.dismiss();
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(mContext,"Some errror just happened",Toast.LENGTH_SHORT).show();
                Log.w("OnFailure",t.toString());
            }
        });
    }
}
