package com.japl600.moviedb.view.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.japl600.moviedb.R;
import com.japl600.moviedb.data.HelperWS;
import com.japl600.moviedb.data.MetodosWS;
import com.japl600.moviedb.data.response.ActorResponse;
import com.japl600.moviedb.data.response.SeriesResponse;
import com.japl600.moviedb.databinding.FragmentMovieDetailBinding;
import com.japl600.moviedb.tools.Constant;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by usuario on 19/03/2018.
 */

public class ActorDetailFragment extends Fragment {

    private Context mContext;
    private ProgressDialog progress;
    private FragmentMovieDetailBinding binding;

    private int actorID;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        progress = new ProgressDialog(mContext);
        progress.setCancelable(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_movie_detail, container, false);
        View view = binding.getRoot();

        //for future uses
        Bundle bundle = getArguments();
        actorID = bundle.getInt("actor_id");

        loadMovieDetail();

        //onClicksListeners
        //binding.btnCancelar.setOnClickListener(this);
        return view;
    }

    private void loadMovieDetail() {
        progress = new ProgressDialog(mContext);
        progress.setMessage("Loading...");
        progress.show();

        //Llamando al WS
        MetodosWS metodosWS = HelperWS.getConfiguration(mContext).create(MetodosWS.class);
        Call<ActorResponse> responseCall = metodosWS.getActorsDetail("person/"+ actorID +"?api_key=ddeff33912ae14df59b1dc36c3a0aaf3&language=en-US");
        responseCall.enqueue(new Callback<ActorResponse>() {

            @Override
            public void onResponse(Call<ActorResponse> call, Response<ActorResponse> response) {
                ActorResponse actorResponse = response.body();
                binding.title.setText(actorResponse.getName());
                binding.subTitle.setText(actorResponse.getBiography());
                Picasso.with(mContext).
                        load(Constant.IMG_ROUTE + actorResponse.getProfile_path()).into(binding.ivBackDrop);
                progress.dismiss();
            }

            @Override
            public void onFailure(Call<ActorResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(mContext,"Some errror just happened",Toast.LENGTH_SHORT).show();
                Log.w("OnFailure",t.toString());
            }
        });
    }
}
