package com.japl600.moviedb.view.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.japl600.moviedb.PrincipalActivity;
import com.japl600.moviedb.R;
import com.japl600.moviedb.adapter.CustomMovieAdapter;
import com.japl600.moviedb.data.HelperWS;
import com.japl600.moviedb.data.MetodosWS;
import com.japl600.moviedb.data.response.PopularMovieResponse;
import com.japl600.moviedb.databinding.FragmentMoviesBinding;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by usuario on 18/03/2018.
 */

public class MoviesFragment extends Fragment {

    private Context mContext;
    private ProgressDialog progress;
    private FragmentMoviesBinding binding;
    private PopularMovieResponse responseBody;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        progress = new ProgressDialog(mContext);
        progress.setCancelable(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_movies, container, false);
        View view = binding.getRoot();

        //for future uses
        Bundle bundle = getArguments();

        //Data de transaccion
        prepareListView();

        loadMovies();

        //onClicksListeners
        //binding.btnCancelar.setOnClickListener(this);
        return view;
    }

    private void prepareListView() {
        binding.lvMovies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((PrincipalActivity) mContext).detailMovie(responseBody.getResults().get(position).getId());

                /*Intent intent = new Intent(mContext, MovieDetailActivity.class);
                intent.putExtra("movie_id",responseBody.getResults().get(position).getId());
				startActivity(intent);*/
            }
        });
    }

    private void loadMovies() {
        progress = new ProgressDialog(mContext);
        progress.setMessage("Loading...");
        progress.show();

        //Llamando al WS
        MetodosWS metodosWS = HelperWS.getConfiguration(mContext).create(MetodosWS.class);
        Call<PopularMovieResponse> responseCall = metodosWS.
                getPopularMovies("1","en-US");
        responseCall.enqueue(new Callback<PopularMovieResponse>() {
            @Override
            public void onResponse(Call<PopularMovieResponse> call, Response<PopularMovieResponse> response) {
                responseBody = response.body();

                if (responseBody != null){
                    Log.w("MoviesFragment",responseBody.getResults().get(0).toString());
                }

                ListAdapter movieAdapter = new CustomMovieAdapter(mContext,R.layout.list_movie, responseBody.getResults());
                binding.lvMovies.setAdapter(movieAdapter);
                progress.dismiss();

            }

            @Override
            public void onFailure(Call<PopularMovieResponse> call, Throwable t) {
                progress.dismiss();
				Toast.makeText(mContext,"Some errror just happened",Toast.LENGTH_SHORT).show();
				Log.w("OnFailure",t.toString());
            }
        });
    }

}
