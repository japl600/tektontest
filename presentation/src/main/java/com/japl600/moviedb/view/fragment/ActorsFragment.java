package com.japl600.moviedb.view.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.japl600.moviedb.PrincipalActivity;
import com.japl600.moviedb.R;
import com.japl600.moviedb.adapter.CustomActorsAdapter;
import com.japl600.moviedb.adapter.CustomSeriesAdapter;
import com.japl600.moviedb.data.HelperWS;
import com.japl600.moviedb.data.MetodosWS;
import com.japl600.moviedb.data.response.PopularActorsResponse;
import com.japl600.moviedb.data.response.PopularSeriesResponse;
import com.japl600.moviedb.databinding.FragmentActorsBinding;
import com.japl600.moviedb.databinding.FragmentSeriesBinding;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by usuario on 19/03/2018.
 */

public class ActorsFragment extends Fragment {

    private Context mContext;
    private ProgressDialog progress;
    private FragmentActorsBinding binding;
    private PopularActorsResponse responseBody;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        progress = new ProgressDialog(mContext);
        progress.setCancelable(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_actors, container, false);
        View view = binding.getRoot();

        //for future uses
        Bundle bundle = getArguments();

        //Data de transaccion
        prepareListView();

        loadActors();

        //onClicksListeners
        //binding.btnCancelar.setOnClickListener(this);
        return view;
    }

    private void prepareListView() {
        binding.lvActors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((PrincipalActivity) mContext).detailActor(responseBody.getResults().get(position).getId());
            }
        });
    }

    private void loadActors() {
        progress = new ProgressDialog(mContext);
        progress.setMessage("Loading...");
        progress.show();

        //Llamando al WS
        MetodosWS metodosWS = HelperWS.getConfiguration(mContext).create(MetodosWS.class);
        Call<PopularActorsResponse> responseCall = metodosWS.getPopularActors("person/popular?api_key=ddeff33912ae14df59b1dc36c3a0aaf3&language=en-US&page=1");
        responseCall.enqueue(new Callback<PopularActorsResponse>() {
            @Override
            public void onResponse(Call<PopularActorsResponse> call, Response<PopularActorsResponse> response) {
                responseBody = response.body();

                ListAdapter movieAdapter = new CustomActorsAdapter(mContext,R.layout.list_movie, responseBody.getResults());
                binding.lvActors.setAdapter(movieAdapter);
                progress.dismiss();

            }

            @Override
            public void onFailure(Call<PopularActorsResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(mContext,"Some errror just happened",Toast.LENGTH_SHORT).show();
                Log.w("OnFailure",t.toString());
            }
        });
    }
}
