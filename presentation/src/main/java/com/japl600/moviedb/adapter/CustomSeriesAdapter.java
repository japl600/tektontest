package com.japl600.moviedb.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.japl600.moviedb.R;
import com.japl600.moviedb.data.response.PopularMovie;
import com.japl600.moviedb.data.response.PopularSerie;
import com.japl600.moviedb.tools.Constant;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by jprada on 08/02/2018.
 */

public class CustomSeriesAdapter extends ArrayAdapter<PopularSerie> {

	private List<PopularSerie> objects;
	private Context context;

	public CustomSeriesAdapter(@NonNull Context context, int resource, @NonNull List<PopularSerie> objects) {
		super(context, resource, objects);
		this.objects = objects;
		this.context = context;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent){
		return getCustomView(position, convertView, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	public View getCustomView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater=(LayoutInflater) context.getSystemService(  Context.LAYOUT_INFLATER_SERVICE );
		View row=inflater.inflate(R.layout.list_movie, parent, false);
		//Title
		TextView title=(TextView)row.findViewById(R.id.title);
		title.setText(objects.get(position).getName());
		//SubTitle
		TextView subTitle=(TextView)row.findViewById(R.id.subTitle);
		subTitle.setText(objects.get(position).getOverview());
		//ImageView
		ImageView imageView = (ImageView)row.findViewById(R.id.ivPoster);
		//Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(imageView);
		Picasso.with(context).load(Constant.IMG_ROUTE + objects.get(position).getPoster_path()).into(imageView);

		/*
		if (position == 0) {//Special style for dropdown header
			title.setTextColor(context.getResources().getColor(R.color.sullana_naranja));
			subTitle.setTextColor(context.getResources().getColor(R.color.sullana_naranja));
		}*/

		return row;
	}

}
